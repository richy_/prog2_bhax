class Szemely {
	String nev;
	int kor;
	char nem; //f-fiu l-lany u-ismeretlen
	int azon;
	static int sorszam = 0;

	Szemely(String _nev, int _kor, char _nem) {
		this.nev = _nev;
		this.kor = _kor;

		if (_nem == 'f' || _nem == 'l') {
			this.nem = _nem;
		} else { 
			this.nem = 'u';
		}
		
		this.azon = sorszam;
		sorszam++;
	}

	@Override
	public String toString() {
		return "Szemely@" + this.nev + "_" + this.kor + "_" + this.nem;
	}

	@Override
	public int hashCode() {
		return this.azon;
	}

	@Override
    public boolean equals(Object obj) {
        Szemely c_sz = (Szemely) obj;
        
        if (this.nev.equals(c_sz.nev) && this.kor == c_sz.kor && this.nem == c_sz.nem) {
            return true;
        } else {
            return false;
        }
	}

}

public class Epam_Object {
	public static void main (String args[]) {

		Szemely sz_Ricsi = new Szemely("Ricsi", 20, 'f');
		Szemely sz_Ricsi2 = new Szemely("Ricsi", 20, 'f');
		Szemely valaki = new Szemely("Valaki", 99, 'l');

		

		System.out.println(sz_Ricsi.toString());
		
		System.out.println(sz_Ricsi.hashCode());
		System.out.println(sz_Ricsi2.hashCode());
		
		System.out.println(sz_Ricsi.equals(sz_Ricsi2));
		System.out.println(sz_Ricsi.equals(valaki));

	}
}