package com.example.final_ttt;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView p1_text;
    private TextView p2_text;
    private Button[][] gombok = new Button[3][3];
    private int kor_szam;
    private int p1_pont;
    private int p2_pont;
    private Button r_gomb;
    private boolean kor;

    private boolean winCondition() {
        String[][] field = new String[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = gombok[i][j].getText().toString();
            }
        }
        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")) {
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")) {
                return true;
            }
        }
        if (field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")) {
            return true;
        }
        if (field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("")) {
            return true;
        }
        return false;
    }

    private void p1Win() {
        p1_pont++;
        Toast.makeText(this, "Player 1 wins!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }
    private void p2Win() {
        p2_pont++;
        Toast.makeText(this, "Player 2 wins!", Toast.LENGTH_SHORT).show();
        updatePointsText();
        resetBoard();
    }
    private void dontetlen() {
        Toast.makeText(this, "Draw!", Toast.LENGTH_SHORT).show();
        resetBoard();
    }

    private void updatePointsText() {
        p1_text.setText("Player 1: " + p1_pont);
        p2_text.setText("Player 2: " + p2_pont);
    }

    private void resetBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                gombok[i][j].setText("");
            }
        }
        kor_szam = 0;
        kor = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p1_text = findViewById(R.id.text_view_p1);
        p2_text = findViewById(R.id.text_view_p2);
        r_gomb = findViewById(R.id.button_reset);
        kor = true;
        kor_szam = 0;
        r_gomb = findViewById(R.id.button_reset);



        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                String buttonID = "button_" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                gombok[i][j] = findViewById(resID);
                gombok[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!((Button) v).getText().toString().equals("")) {
                            return;
                        }

                        if (kor) {
                            ((Button) v).setText("X");
                        }

                        if (!kor) {
                            ((Button) v).setText("O");
                        }

                        kor_szam++;

                        if (winCondition()) {
                            if (kor) {
                                p1Win();
                            } else {
                                p2Win();
                            }
                        } else if (kor_szam== 9) {
                            dontetlen();
                        } else {
                            kor = !kor;
                        }

                    }
                });
            }

        }

        r_gomb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBoard();
            }
        });

    }


}