public class portscan {
    
    public static void main(String[] args) {
        
        for(int i=0; i<1024; ++i)
            
            try {
                
                java.net.Socket socket = new java.net.Socket(args[0], i);
                
                System.out.println(i + " figyeli");
                
                socket.close();
                
            } catch (Exception e) {
                
                System.out.println(i + " nem figyeli");
                
            }
    }
}

/*
A program egy for cikluson keresztül vizsgálja meg, hogy a host gép figyeli-e a
az adott portokat a 0-1024 tartományban. Ezt úgy teszi meg, hogy a for ciklus minden
iterációjában létrehoz egy új socket (hálózati végpont) objektumot, ami megpróbál a host i.-k portjára
kapcsolódni. 

A socket osztály ezen konstruktora 2 paramétert vár:
a host IP-címét, amely parancssori argumentumként adható meg futtatás során (például a localhost
esetén a 127.0.0.1) illetve egy port számot, amit a fent említett ciklus iterál.
Ez a konstruktor ugyanakkor több exceptiont (kivételt) is dob, ezért a cikluson belül a try-catch
utasítás blokkokban történik a socket kétrehozása.

A konstruktor által dobott 'UnknownHostException' arra utal, hogy nem tudott az adott IP-címen és porton
kapcsolatot létrehozni, ami arra utal, hogy az adott host nem figyeli azt a portot, illetve ha nem tud kapcsolatot
létrehozni, akkor nyilván a .close() metódus sem tudja azt megszüntetni, így az I/O Exceptiont fog adni.
Ezt a két kivételt kihasználva már világos a program működése:

- amennyiben a socketet sikeresen létrehozta akkor azt rögtön be is zárja, és a "figyeli" választ küldi.
- ha nem sikerül létrehozni a socketet valamilyen exception miatt, akkor a catch blokkban egyszerűen a "nem figyeli"
    üzenetet fogja kiírni.
*/
    
