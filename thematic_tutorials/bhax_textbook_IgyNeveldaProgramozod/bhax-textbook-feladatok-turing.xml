<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Turing!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Végtelen ciklus</title>
        <para>
            Írj olyan C végtelen ciklusokat, amelyek 0 illetve 100 százalékban dolgoztatnak egy magot és egy olyat, amely  
            100 százalékban minden magot!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/0LWSgXj41FI">YouTube.com/R!chY</link>
        </para>
        <para>
            Megoldás forrása (0%): <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/vegtelen0.c">gitlab.com/richy_
            </link>, 
            Megoldás forrása (100%): <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/vegtelen100.c">gitlab.com/richy
            </link>.
        </para>
        <para>
            Számos módon hozhatunk és hozunk létre végtelen ciklusokat. 
            Vannak esetek, amikor ez a célunk, például egy szerverfolyamat fusson folyamatosan és van amikor egy
            bug, mert ott lesz végtelen ciklus, ahol nem akartunk. Saját péláinkban ilyen amikor a PageRank algoritmus
            rázza az 1 liter vizet az internetben, de az iteráció csak nem akar konvergálni...
        </para>                    
        <para>
            Egy mag 100 százalékban:               
        </para>
        <programlisting language="c"><![CDATA[int
main ()
{
  for (;;);

  return 0;
}
]]>
        </programlisting>        
        <para>        
        vagy az olvashatóbb, de a programozók és fordítók (szabványok) között kevésbé hordozható
        </para>
        <programlisting language="c"><![CDATA[int
#include <stdbool.h>
main ()
{
  while(true);

  return 0;
}]]>
        </programlisting>        
        <para>
            Azért érdemes a <literal>for(;;)</literal> hagyományos formát használni, 
            mert ez minden C szabvánnyal lefordul, másrészt
            a többi programozó azonnal látja, hogy az a végtelen ciklus szándékunk szerint végtelen és nem szoftverhiba. 
            Mert ugye, ha a <literal>while</literal>-al trükközünk egy nem triviális 
            <literal>1</literal> vagy <literal>true</literal> feltétellel, akkor ott egy másik, a forrást
            olvasó programozó nem látja azonnal a szándékunkat.
        </para>            
        <para>
            Egy mag 0 százalékban:               
        </para>
        <para>        
        <programlisting language="c"><![CDATA[#include <unistd.h>
int
main ()
{
  for (;;)
    sleep(1);
    
  return 0;
}
]]>
        </programlisting>
    </para>
        <para>
            A futtatás közben a top programmal monitoroztam a CPU használatot, jól látható, hogy semmi kiugró nem tapasztalható a program futtatása során:

        <mediaobject>
                <imageobject>
                    <imagedata fileref="img/vegtelen0.png" scale="50" />
                </imageobject>
        </mediaobject>
        </para>        
        <para>
            Minden mag 100 százalékban:               

        <programlisting language="c"><![CDATA[#include <omp.h>
int
main ()
{
#pragma omp parallel
{
  for (;;);
}
  return 0;
}]]>
        </programlisting>
    </para>
        <para>
            Ez egy jó példa arra, hogyan lehet nagy erőforrást igénylő processeket futtatni az összes (virtuális) magon.
        </para>
        <para>A top ebben az esetben jól látható különbségeket mutatott...
        <mediaobject>
        <imageobject>
                <imagedata fileref="img/vegtelen100.png" scale="50" />
        </imageobject>
    </mediaobject>
        </para>
        <tip>
            <title>Akkor ez most rossz?!</title>
                    <para>
                        A végtelen ciklus nem feltétlenül jelent rosszat (amennyiben feature és nem BUG). Számos esetben, mint például a későbbi "labdapattogás" feladat során is kifejezetten hasznunkra válik!
                    </para>                 
        </tip>            
    </section>        
        
    <section>
        <title>Lefagyott, nem fagyott, akkor most mi van?</title>
        <para>
            Mutasd meg, hogy nem lehet olyan programot írni, amely bármely más programról eldönti, hogy le fog-e fagyni vagy sem!
        </para>
        <para>
            Megoldás videó:
        </para>
        <para>
            Megoldás forrása:  tegyük fel, hogy akkora haxorok vagyunk, hogy meg tudjuk írni a <function>Lefagy</function>
            függvényt, amely tetszőleges programról el tudja dönteni, hogy van-e benne vlgtelen ciklus:              
        </para>
        <programlisting language="c"><![CDATA[Program T100
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	main(Input Q)
	{
		Lefagy(Q)
	}
}]]></programlisting>            
        <para>
            A program futtatása, például akár az előző <filename>v.c</filename> ilyen pszeudókódjára:
            <screen><![CDATA[T100(t.c.pseudo)
true]]></screen>            
            akár önmagára
            <screen><![CDATA[T100(T100)
false]]></screen>  
            ezt a kimenetet adja.          
        </para>
        <para>
            A T100-as programot felhasználva készítsük most el az alábbi T1000-set, amelyben a
            Lefagy-ra épőlő Lefagy2 már nem tartalmaz feltételezett, csak csak konkrét kódot:
        </para>
        <programlisting language="c"><![CDATA[Program T1000
{

	boolean Lefagy(Program P)
	{
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	}

	boolean Lefagy2(Program P)
	{
		 if(Lefagy(P))
			return true;
		 else
			for(;;); 
	}

	main(Input Q)
	{
		Lefagy2(Q)
	}

}]]></programlisting>            
        <programlisting><![CDATA[]]></programlisting>            
        <para>
            Mit for kiírni erre a <computeroutput>T1000(T1000)</computeroutput> futtatásra?
                                
            <itemizedlist>
                <listitem>
                    <para>Ha T1000 lefagyó, akkor nem fog lefagyni, kiírja, hogy true</para>                        
                </listitem>
                <listitem>
                    <para>Ha T1000 nem fagyó, akkor pedig le fog fagyni...</para>                        
                </listitem>
            </itemizedlist>
            akkor most hogy fog működni? Sehogy, mert ilyen <function>Lefagy</function>
            függvényt, azaz a T100 program nem is létezik.                
        </para>
        <para>
            Hasonlítsuk a problémát egy "hétköznapi" paradoxonhoz, hogy könnyebben megérthessük:
        </para>
        <para>
            Pinokkió a következőt állítja: "Meg fog nőni az orrom", ha igazat mondott, akkor nem nő az orra, tehát nem fog megnőni, ezáltal az állítás hamis volt. Ugyanakkor ha az állítás hamis, akkor Pinokkió orra megnő, tehát mégis csak igaz volt az állítás. Ha igaz volt akkor az előbb azt mondtam hogy hamis volt, de ha hamis akkor igaz, tehát sikerült magamat belekergetni egy végtelen ciklusba amikoris folyamatosan ellentmondok saját magamnak, akárcsak a T1000-es program :) 
        </para>
    </section>        
                
    <section>
        <title>Változók értékének felcserélése</title>
        <para>
            Írj olyan C programot, amely felcseréli két változó értékét, bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://www.youtube.com/watch?v=b5lghJWxbYQ">YouTube.com/R!chY</link>
        </para>
        <para>
            Megoldás forrása:  <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/valtcsere.c">gitlab.com/richy_</link>
        </para>
        <para>
            Két változó értékét segédváltozók bevezetése nélkül is fel tudjuk cserélni számos matematikai művelettel, vagy akár a "kizáró vagy" operátor használatával. Ezekről már tanultunk az első félév során a Bevezetés a programozásba nevű tárgy keretein belül, illetve a második féléves Adatszerkezetek és algoritmusok gyakorlatain is. Ezt a feladatot az összeadás és kivonás műveletekkel oldottam meg ebben az esetben.
        </para>
        <programlisting language="c"><![CDATA[
a = a+b;
b = a-b;
a = a-b;]]>
        </programlisting>     
    </section>                     

    <section>
        <title>Labdapattogás</title>
        <para>
            Először if-ekkel, majd bármiféle logikai utasítás vagy kifejezés
            nasználata nélkül írj egy olyan programot, ami egy labdát pattogtat a karakteres konzolon! (Hogy mit értek
            pattogtatás alatt, alább láthatod a videókon.)
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://www.youtube.com/watch?v=aBwgVnWc37M">YouTube.com/R!chY</link>
        </para>
        <para>
            Megoldás videó (IF nélkül): <link xlink:href="https://www.youtube.com/watch?v=wgBVBM4uUeM">YouTube.com/R!chY</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/ballif.c">gitlab.com/richy_</link>
        </para>
        <para>
            Megoldás forrása (IF nélkül): <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/ballnoif.c">gitlab.com/richy_</link>
        </para>
        <para>
            A változócseréhez hasonlóan ezzel a problémával is foglalkoztunk az első félév során.
        </para>
        <para>
            Elágazó utasítás használata esetén minden iteráció során a labda x és y pozícióját megnövelem az előre definiált sebességgel. Amikor a labda x/y koordinátája eléri a maximális értéket (vagy épp a minimális 0-t) akkor a sebességét (-1)-el szorozva a labda ellentétes irányban halad tovább.
            <programlisting><![CDATA[
x = y + xspeed;
y = y + yspeed;

if (x >= width+1) {
    xspeed = xspeed * -1;
}

if (x <=0) {
    xspeed = xspeed * -1;
}

if (y <=0) {
    yspeed = yspeed * -1;
}

if (y >= height+1) {
    yspeed = yspeed * -1;
}
                
]]></programlisting>  
        </para>
        <para>
            Az IF utasítás nélküli verzióban a labda megjelenítése/kirajzolása az előző megoldáshoz hasonlóan az alábbi függvénnyel történik:
            <programlisting><![CDATA[
int labda(int x, int y) {
    int i;

    for(i=0; i<x; i++) {
        printf("\n");
    }

    for(i=0; i<y; i++) {
        printf(" ");
    }

    printf("O\n");

    return 0;
}

]]></programlisting>
        </para>
        <para>
            Eltérést a main függvényben találunk, ahol az irányváltást nem feltételes utasításokkal vezéreljük, hanem az abs és modulo matematikai függvényekkel:
        <programlisting><![CDATA[
    labda(abs(height-(x++%(height*2))), abs(width-(y++%(width*2))));
    ]]></programlisting>
        </para>
    </section>                     

    <section>
        <title>Szóhossz és a Linus Torvalds féle BogoMIPS</title>
        <para>
            Írj egy programot, ami megnézi, hogy hány bites a szó a gépeden, azaz mekkora az <type>int</type> mérete.
            Használd ugyanazt a while ciklus fejet, amit Linus Torvalds a BogoMIPS rutinjában! 
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://www.youtube.com/watch?v=261q-JEFuj8">YouTube.com/R!chY</link>, 
        </para>
        <para>
        	BogoMIPS videó: <link xlink:href="https://www.youtube.com/watch?v=rwJ55707_aM">YouTube.com/R!chY</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/bogomips.c">gitlab.com/richy_
            </link>
        </para>
        <para>
            Megoldás forrása (szóhossz): <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/shift.c">gitlab.com/richy_</link>
        </para>
        <para>
            A változók tárolásához meghatározott mennyiségű memóriára van szükség, hogy pontosan mennyire egy integer esetén, azt kiderítjük ebben a feladatban a <![CDATA[<<]]> operátor segítségével...
            A while ciklusban a <![CDATA[<<]]> operátor addig tolja el az integer bitjeit balra, amíg el nem éri a 0 értéket.
            <programlisting><![CDATA[
while (a != 0) {
    hanyszor++;
    a = a << 1;
}]]>
</programlisting>
        </para>
        <para>
            Outputként a 32-es számot kapjuk, tehát megállapíthatjuk, hogy 1 integer mérete 4 bájt.
        </para>
        <para>
            A (Bogo)MIPS lényege, a "Milliónyi -üres- utasítás másodpercenként". Ebből következtethetünk a Bogo/bogus jelentésére is, ami annyit takar, hogy haszontalan, mivel a ciklus során a CPU üres várakoztató (no-op) utasításokat kap.
        </para>
        <para>
            A mai korszerű benchmark programokkal ellentétben nem használható releváns összehasonlításra, mindenesetre beszúrom ide az én eredményem, amit a kollégiumi laptopom produkált :)
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/bogo.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A B<subscript>2</subscript> konstans közelítése</phrase>
                </textobject>
            </mediaobject>
        </para>
    </section>                     

    <section>
        <title>Helló, Google!</title>
        <para>
            Írj olyan C programot, amely egy 4 honlapból álló hálózatra kiszámolja a négy lap Page-Rank 
            értékét!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://www.youtube.com/watch?v=QrSP0UlrnPQ">YouTube.com/R!chY</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/richy_/prog1_sources/-/blob/master/turing/pagerank.c">gitlab.com/richy_</link>
        </para>
        <para>
            A PageRank algoritmus a Google kereső alapja, amely a webldalak közti hivatkozást is figyelembe véve állít rangsort az egyes oldalak közt.
        </para>
        <para>
            A main függvényben inicializáljuk a 3 tömböt amelyekből a rangsort fogjuk számolni:
            <programlisting><![CDATA[
double L[4][4] = {
    {0.0, 0.0, 1.0 / 3.0, 0.0},
    {1.0, 1.0 / 2.0, 1.0 / 3.0, 1.0},
    {0.0, 1.0 / 2.0, 0.0, 0.0},
    {0.0, 0.0, 1.0 / 3.0, 0.0}
};
    double PR[4] = {0.0, 0.0, 0.0, 0.0};
    double PRv[4] = {1.0/4.0,1.0/4.0,1.0/4.0,1.0/4.0};]]>
</programlisting>
        Az L nevű kétdimenziós tömb tárolja az egyes oldalak közti hivatkozás érékeket. Minden oldal 1 egységnyi "szavazatot" oszthat szét a többi oldal közt. A PR tömb fogja tárolni az előző iteráció során számított értékeket. A PRv az iterációk közti állapotot rögzíti.
        </para>
        <para>
            A PageRank számítást egy for ciklus végzi, mindaddig amíg az iterációk közti érték eltérés egy bizonyos "küszöbérték" alá nem csökken, ekkor a break; utasítás megszakítja a ciklust, és kiírathatjuk az értékeket.
            <programlisting><![CDATA[
for(;;) {

    for (i = 0; i < 4; i++) {
        PR[i] = PRv[i];
    }

    for (i = 0; i<4; i++) {
        double temp = 0;

        for (j = 0; j<4; j++) {
            temp+=L[i][j]*PR[j];
        }

        PRv[i] = temp;
        
    }

    if (tavolsag(PR, PRv) < 0.00001)
        break;
}

kiir (PR);]]>
    </programlisting>
            </para>
    <para>Output:<programlisting><![CDATA[
0.090908
0.545453
0.272730
0.090908]]>
</programlisting>
    </para>
    </section>
                                                                                                                                                                                                                                                                                                                                                        
    <section xml:id="bhax-textbook-feladatok-turing.MontyHall">
        <title>A Monty Hall probléma</title>
        <para>
            Írj R szimulációt a Monty Hall problémára!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/eZhsVRnSBk0">YouTube.com/R!chY</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R</link>
        </para>
        <para>
            A probléma egy amerikai TV-showból ered, amelynek műsorvezetőjét Monty Hallnak hívták. A probléma lényege, hogy 3 zárt ajtó közül választhatunk, amelykből az egyik mögött értékes nyeremény található, még a másik 2 mögött értéktelen dolog.
        </para>
        <para>
            A játék során választanunk kell egyet a három zárt ajtó közül, majd a műsorvezető kinyitja a fennmaradó 2 ajtó közül az egyiket, amely mögött BIZTOSAN nem a főnyeremény rejtőzik. Ekkor két választásunk van: kitartunk az első választásunk mellett, amely 1/3-ad eséllyel rejti a nyereményt, vagy változtatunk és 50-50 alapon a másik ajtóra voksolunk. A probléma egyszerű valószínűség számítással is bizonyítható, hogy érdemesebb mindig változtatni a tippünkön, hiszen összességében nagyobb eséllyel találhatjuk el a főnyereményt. Ezt a videóban is bemutattam egy egyszerű táblázat segítségével, ahol a váltás esetén az összes lehetséges esetek száma 9, ebből 6 eset kedvező számunkra, tehát 6/9, vagyis 2/3 esélyünk van a nyeremény eltrafálására.
        </para>
        <para>
        A program ezen részében inicializáljuk a kísérletek számát, az adott kísérlet nyertes ajtaját, a játékost és a műsorvezetőt.
            <programlisting><![CDATA[
kiserletek_szama=10000000
kiserlet = sample(1:3, kiserletek_szama, replace=T)
jatekos = sample(1:3, kiserletek_szama, replace=T)
musorvezeto=vector(length = kiserletek_szama)]]>          
            </programlisting>
        </para>
        <para>
            A szimuláció érdemi részét egy for() ciklusban találjuk, ami az előre beállított kísérletek számávan egyező alkalommal fut le.
            <programlisting><![CDATA[
for (i in 1:kiserletek_szama) {

    if(kiserlet[i]==jatekos[i]){
    
        mibol=setdiff(c(1,2,3), kiserlet[i])
    
    } else {
    
        mibol=setdiff(c(1,2,3), c(kiserlet[i], jatekos[i]))
    
    }

    musorvezeto[i] = mibol[sample(1:length(mibol),1)]

}

nemvaltoztatesnyer= which(kiserlet==jatekos)
valtoztat=vector(length = kiserletek_szama)

for (i in 1:kiserletek_szama) {

    holvalt = setdiff(c(1,2,3), c(musorvezeto[i], jatekos[i]))
    valtoztat[i] = holvalt[sample(1:length(holvalt),1)]
    
}

valtoztatesnyer = which(kiserlet==valtoztat)
]]>         </programlisting>
        Az if() - else() ágban dől el, hogy a játékos választásától függően hány ajtóból választhat a műsorvezető. (Rossz választás esetén 1, helyes választás esetén 2.)
        A műsorvezető ajtónyitása után megnézzük, hogy nyerne-e a játékos ha váltana, vagy veszítene.
        </para>
        <para>
            Ezt követi egy kiíratás a program végén, az alábbi outputtal:
            <programlisting><![CDATA[
[1] "Kiserletek szama: 1000"
[1] 323
[1] 677
[1] 0.4771049
[1] 1000]]></programlisting>
        *A futtatáshoz az online <link xlink:href="https://rextester.com/l/r_online_compiler">Rextester R compilert></link> használtam, ahol limitálva volt a maximális kísérletek száma erőforrásból eredő okokból.*
        </para>
        <tip>
            <title>Kipróbálnád a játékot?</title>
            <para><link xlink:href="http://www.math.ucsd.edu/~crypto/Monty/monty.html">Ezen</link> az internetes portálon Te is kipróbálhatod:)
            </para>
            <mediaobject>
            <imageobject>
                <imagedata fileref="img/monty.png" scale="25" />
        </imageobject>
    </mediaobject>
        </tip>
    </section>

    <section xml:id="Brun">
        <title>100 éves a Brun tétel</title>
        <para>
            Írj R szimulációt a Brun tétel demonstrálására!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/xbYhp9G6VqQ">https://youtu.be/xbYhp9G6VqQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R</link>
        </para>

        <para>
            A természetes számok építőelemei a prímszámok. Abban az értelemben, 
            hogy minden természetes szám előállítható prímszámok szorzataként.
            Például 12=2*2*3, vagy például 33=3*11.
        </para>
        <para>
            Prímszám az a természetes szám, amely csak önmagával és eggyel 
            osztható. Eukleidész görög matematikus már Krisztus előtt tudta, 
            hogy végtelen sok prímszám van, de ma sem tudja senki, hogy 
            végtelen sok ikerprím van-e. Két prím ikerprím, ha különbségük 2.
        </para>
        <para>
            Két egymást követő páratlan prím között a legkisebb távolság a 2, 
            a legnagyobb távolság viszont bármilyen nagy lehet! Ez utóbbit 
            könnyű bebizonyítani. Legyen n egy tetszőlegesen nagy szám. 
            Akkor szorozzuk össze n+1-ig a számokat, azaz számoljuk ki az 
            1*2*3*… *(n-1)*n*(n+1) szorzatot, aminek a neve (n+1) faktoriális, 
            jele (n+1)!.
        </para>
        <para>
            Majd vizsgáljuk meg az a sorozatot:
        </para>    
        <para>
            (n+1)!+2, (n+1)!+3,… , (n+1)!+n, (n+1)!+ (n+1) ez n db egymást követő azám, ezekre (a jól ismert
            bizonyítás szerint) rendre igaz, hogy            
        </para>    
        <itemizedlist>
            <listitem>
                <para>(n+1)!+2=1*2*3*… *(n-1)*n*(n+1)+2, azaz 2*valamennyi+2, 2 többszöröse, így ami osztható kettővel</para>
            </listitem>
            <listitem>
                <para>(n+1)!+3=1*2*3*… *(n-1)*n*(n+1)+3, azaz 3*valamennyi+3, ami osztható hárommal</para>
            </listitem>
            <listitem>
                <para>...</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n-1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n-1)*valamennyi+(n-1), ami osztható (n-1)-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+n=1*2*3*… *(n-1)*n*(n+1)+n, azaz n*valamennyi+n-, ami osztható n-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n+1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n+1)*valamennyi+(n+1), ami osztható (n+1)-el</para>
            </listitem>
        </itemizedlist>
        <para>
            tehát ebben a sorozatban egy prim nincs, akkor a (n+1)!+2-nél 
            kisebb első prim és a (n+1)!+ (n+1)-nél nagyobb első 
            prim között a távolság legalább n.            
        </para>    
        <para>
            Az ikerprímszám sejtés azzal foglalkozik, amikor a prímek közötti 
            távolság 2. Azt mondja, hogy az egymástól 2 távolságra lévő prímek
            végtelen sokan vannak.
        </para>    
        <para>
            A Brun tétel azt mondja, hogy az ikerprímszámok reciprokaiból képzett sor összege, azaz
            a (1/3+1/5)+ (1/5+1/7)+ (1/11+1/13)+... véges vagy végtelen sor konvergens, ami azt jelenti, hogy ezek
            a törtek összeadva egy határt adnak ki pontosan vagy azt át nem lépve növekednek, 
            ami határ számot B<subscript>2</subscript> Brun konstansnak neveznek. Tehát ez
            nem dönti el a több ezer éve nyitott kérdést, hogy az ikerprímszámok halmaza végtelen-e? 
            Hiszen ha véges sok van és ezek
            reciprokait összeadjuk, akkor ugyanúgy nem lépjük át a B<subscript>2</subscript> Brun konstans értékét, 
            mintha végtelen 
            sok lenne, de ezek már csak olyan csökkenő mértékben járulnának hozzá a végtelen sor összegéhez, 
            hogy így sem lépnék át a Brun konstans értékét.     
        </para>
        <para>
            Ebben a példában egy olyan programot készítettünk, amely közelíteni próbálja a Brun konstans értékét.
            A repó <link xlink:href="../../../bhax/attention_raising/Primek_R/stp.r">
                <filename>bhax/attention_raising/Primek_R/stp.r</filename>
            </link> mevű állománya kiszámolja az ikerprímeket, összegzi
            a reciprokaikat és vizualizálja a kapott részeredményt.
        </para>
        <programlisting language="R">
<![CDATA[#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

library(matlab)

stp <- function(x){

    primes = primes(x)
    diff = primes[2:length(primes)]-primes[1:length(primes)-1]
    idx = which(diff==2)
    t1primes = primes[idx]
    t2primes = primes[idx]+2
    rt1plust2 = 1/t1primes+1/t2primes
    return(sum(rt1plust2))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")
]]>
        </programlisting>        
        <para>
            Soronként értelemezzük ezt a programot:
        </para>                
        <programlisting language="R">
<![CDATA[ primes = primes(13)]]>
        </programlisting>        
        <para>
            Kiszámolja a megadott számig a prímeket.             
        </para>
        <screen>
<![CDATA[> primes=primes(13)
> primes
[1]  2  3  5  7 11 13
]]>
        </screen>
                
        <programlisting language="R">
<![CDATA[ diff = primes[2:length(primes)]-primes[1:length(primes)-1]]]>
        </programlisting>        
        <screen>
<![CDATA[> diff = primes[2:length(primes)]-primes[1:length(primes)-1]
> diff
[1] 1 2 2 4 2
]]>
        </screen>        
        <para>
            Az egymást követő prímek különbségét képzi, tehát 3-2, 5-3, 7-5, 11-7, 13-11.
        </para>
        <programlisting language="R">
<![CDATA[idx = which(diff==2)]]>
        </programlisting>        
        <screen>
<![CDATA[> idx = which(diff==2)
> idx
[1] 2 3 5
]]>
        </screen>              
        <para>
            Megnézi a <varname>diff</varname>-ben, hogy melyiknél lett kettő az eredmény, mert azok az ikerprím párok, ahol ez igaz.
            Ez a <varname>diff</varname>-ben lévő 3-2, 5-3, 7-5, 11-7, 13-11 külünbségek közül ez a 2., 3. és 5. indexűre teljesül.
        </para>
        <programlisting language="R">
<![CDATA[t1primes = primes[idx]]]>
        </programlisting>  
        <para>
            Kivette a primes-ból a párok első tagját. 
        </para>
        <programlisting language="R">
<![CDATA[t2primes = primes[idx]+2]]>
        </programlisting>        
        <para>
            A párok második tagját az első tagok kettő hozzáadásával képezzük.
        </para>
        <programlisting language="R">
<![CDATA[rt1plust2 = 1/t1primes+1/t2primes]]>
        </programlisting>        
        <para>
            Az 1/t1primes a t1primes 3,5,11 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t1primes
[1] 0.33333333 0.20000000 0.09090909
]]>
        </screen>                      
        <para>
            Az 1/t2primes a t2primes 5,7,13 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t2primes
[1] 0.20000000 0.14285714 0.07692308
]]>
        </screen>                      
        <para>
            Az 1/t1primes + 1/t2primes pedig ezeket a törteket rendre összeadja.
        </para>
        <screen>
<![CDATA[> 1/t1primes+1/t2primes
[1] 0.5333333 0.3428571 0.1678322
]]>
        </screen>                      
        <para>
            Nincs más dolgunk, mint ezeket a törteket összeadni a 
            <function>sum</function> függvénnyel.
        </para>
        
        <programlisting language="R">
<![CDATA[sum(rt1plust2)]]>
        </programlisting>    
        <screen>
<![CDATA[>   sum(rt1plust2)
[1] 1.044023
]]>
        </screen>            
        <para>
            A következő ábra azt mutatja, hogy a szumma értéke, hogyan nő, egy határértékhez tart, a 
            B<subscript>2</subscript> Brun konstanshoz. Ezt ezzel a csipettel rajzoltuk ki, ahol először a fenti 
            számítást 13-ig végezzük, majd 10013, majd 20013-ig, egészen 990013-ig, azaz közel 1 millióig.
            Vegyük észre, hogy az ábra első köre, a 13 értékhez tartozó 1.044023.
        </para>
        <programlisting language="R">
<![CDATA[x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")]]>
        </programlisting>          
        <figure>
            <title>A B<subscript>2</subscript> konstans közelítése</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/BrunKorok.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A B<subscript>2</subscript> konstans közelítése</phrase>
                </textobject>
            </mediaobject>
        </figure>                             
    </section>

</chapter>                
