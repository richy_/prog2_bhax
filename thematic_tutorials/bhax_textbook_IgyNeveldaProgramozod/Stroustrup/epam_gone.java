import java.io.Writer;
import java.io.FileWriter;
import java.io.IOException;


class BugousStuffProducer {
	private final Writer writer;
	
	public BugousStuffProducer(String outputFileName) throws IOException {
		writer = new FileWriter(outputFileName);
	}

	public void writeStuff() throws IOException {
		writer.write("Stuff");
		writer.flush();
	}
	
	@Override
	public void finalize() throws IOException {
		writer.close();
		System.out.println("Finalized!");
	}
}


public class epam_gone {

	public static void main(String[] args) throws IOException {
		BugousStuffProducer bsp = new BugousStuffProducer("asd.txt");
		bsp.writeStuff();
		//bsp.finalize();
	}

}