#include <iostream>
#include <string>
#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

class Reader {
    
    public:
        
        int counter;

        Reader()
        {
            counter = 0;
        }

        void readClasses(path path)
        {
        
            if (is_regular_file(path))
            {
                string ext(".java");
                if (!ext.compare(extension(path))) //compare returns 0 if match
                {
                    cout << path.string() << endl;
                    counter++;
                }
            }
            else if (is_directory(path)) {
                for (directory_entry &entry : directory_iterator(path))
                {
                    readClasses(entry.path());
                }
            }
        }

        int getCounter()
        {
            return counter;
        }
};

int main() {

    Reader javaClass;
    javaClass.readClasses("java_src");
    cout << "\nJDK osztályok száma:" << javaClass.getCounter() << "\n";
    return 0;

}