/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caesarencoding;

/**
 *
 * @author RichY
 */
public class encoder {
    private static char[] karakterek = {'a', 'b', 'c', 'd', 'e', 
            'f', 'g', 'h', 'i', 'j', 'k', 'l', 
            'm', 'n', 'o', 'p', 'q', 'r', 's', 
            't', 'u', 'v', 'w', 'x', 'y', 'z'}; //26

    
    public static String translate(int shift, String input) {
        StringBuilder output = new StringBuilder();
        
        for (int i = 0; i < input.length(); i++) {
            
            boolean found = false;
            
            for (int j = 0; j < karakterek.length; j++) {
                if (karakterek[j] == input.charAt(i)) {
                    char c = karakterek[(j+shift)%26];
                    output.append(c);
                    found = true;
                }
            }
                
            if (!found) {
                output.append(input.charAt(i));
            }
        }
        
        return output.toString();
        
    }
  
}

