
package caesarencoding;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CaesarEncoding {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String input;
        
        FileWriter writer = new FileWriter("kimenet.txt");
        BufferedWriter bfw = new BufferedWriter(writer);
        
        int shift_c = sc.nextInt();
        
        while (true) {
            input = sc.nextLine();
            
            if (input.equals("|")) {
                bfw.flush();
                bfw.close();
                break;
            }

            //System.out.println(encoder.translate(3, input));
            bfw.write(encoder.translate(shift_c, input) + "\n");
        }
    }
    
}
