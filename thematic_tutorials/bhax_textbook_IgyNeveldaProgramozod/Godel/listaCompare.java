import java.util.ArrayList;
import java.util.LinkedList;


public class listaCompare {

      public static void perf_arraylistAddFirst() {
            ArrayList<Integer> alist = new ArrayList<Integer>();
            
            for (int i = 0; i < 200000; i++) {
                  alist.add(0, i);
            }
      }
      
      public static void perf_arraylistAddLast() {
            ArrayList<Integer> alist = new ArrayList<Integer>();
            
            for (int i = 0; i < 200000; i++) {
                  alist.add(i);
            }
      }
      
      public static void perf_arraylistGetHalf() {
            ArrayList<Integer> alist = new ArrayList<Integer>();
            
            for (int i = 0; i < 2000000; i++) {
                  alist.add(i);
            }
            
            alist.get(2000000/2);
      }
      
      public static void perf_linkedlistAddFirst() {
            LinkedList<Integer> llist = new LinkedList<Integer>();
            
            for (int i = 0; i < 200000; i++) {
                  llist.add(0, i);
            }
      }
      
      public static void perf_linkedlistAddLast() {
            LinkedList<Integer> llist = new LinkedList<Integer>();
            
            for (int i = 0; i < 200000; i++) {
                  llist.add(i);
            }
      }
      
      public static void perf_linkedlistGetHalf() {
            LinkedList<Integer> llist = new LinkedList<Integer>();
            
            for (int i = 0; i < 2000000; i++) {
                  llist.add(i);
            }
            
            llist.get(2000000/2);
      }
      
      
      
      public static void main(String[] args) {
            
            //perf_arraylistAddFirst();
            //perf_arraylistAddLast();
            //perf_linkedlistAddFirst();
            //perf_linkedlistAddLast();
            //perf_linkedlistGetHalf();
            //perf_arraylistGetHalf();
            
      }
}