public class lambda {

	public static void scareSomething(Scary sc) {
		sc.scare();
	}

	public static void main(String[] args) {
		
		Scary clownScare = () -> {
			System.out.println("A clown SCARED you!");
		};

		scareSomething(clownScare);

	}

}