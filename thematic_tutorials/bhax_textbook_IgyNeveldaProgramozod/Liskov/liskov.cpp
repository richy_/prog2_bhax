#include<iostream>

class Teglalap {
	
	public:
		int magassag;
		int szelesseg;
		
		void setMagassag(int _magassag){
			magassag = _magassag;
		}

		void setSzelesseg(int _szelesseg){
			szelesseg = _szelesseg;
		}		
		
		int getTerulet(){
			return magassag*szelesseg;
		}
};
		
class Negyzet : public Teglalap{
	public:
		
		void setMagassag(int _magassag){
			szelesseg = _magassag;
			magassag = _magassag;
		}
		
		void setSzelesseg(int _szelesseg){
			szelesseg = _szelesseg;
			magassag = _szelesseg;
		}

		int getTerulet(){
			return magassag*szelesseg;
		}

};

int main () {
	
	Teglalap t1;
	t1.setMagassag(5);
	t1.setSzelesseg(10);
	std::cout << t1.getTerulet() << std::endl;

	Negyzet n1;
	n1.setMagassag(5);
	n1.setSzelesseg(10);
	std::cout << n1.getTerulet() << std::endl;


}