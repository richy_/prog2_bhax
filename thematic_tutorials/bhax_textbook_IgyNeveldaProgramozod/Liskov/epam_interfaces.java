interface epamIf {
	public default void sayHello() {
		System.out.println("Interface vagyok!");
	}

	static void epamHello() {
		System.out.println("EPAM interface vagyok!");
	}
}

class epamOsztaly implements epamIf {
	public void sayHello(){
		System.out.println("Egyedi implementacio vagyok!");
	}
}

public class epam_interfaces implements epamIf {

	public static void main(String[] args) {

		epamOsztaly eo = new epamOsztaly();

		eo.sayHello();
		epamIf.epamHello();

	}
}