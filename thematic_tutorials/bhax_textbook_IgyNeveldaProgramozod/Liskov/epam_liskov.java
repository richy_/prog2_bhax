class Vehicle {
	
	public Vehicle() {
		System.out.println("Vehicle constructor");
	}

	public void start() {
		System.out.println("Vehicle started!");
	}

}

class Car extends Vehicle {
	
	public Car() {
		System.out.println("Car constructor");
	}

	public void start() {
		System.out.println("Car started!");
	}

}

class Supercar extends Car {
	
	public Supercar() {
		System.out.println("Supercar constructor");
	}

	public void start() {
		System.out.println("Supercar started!");
	}

}

public class epam_liskov {
	public static void main (String[] args) {
		
		//1
		Vehicle firstVehicle = new Supercar();
		
		//2
		firstVehicle.start();
		
		//3
		System.out.println(firstVehicle instanceof Car);
		
		//4
		Car secondVehicle = (Car) firstVehicle;
		
		//5
		secondVehicle.start();
		
		//6
		System.out.println(secondVehicle instanceof Supercar);
		
		//7
		Supercar thirdVehicle = new Vehicle();

		//8
		thirdVehicle.start();
	}
}