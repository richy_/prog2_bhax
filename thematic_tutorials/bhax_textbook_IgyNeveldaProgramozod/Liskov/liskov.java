class Teglalap {
	int szelesseg;
	int magassag;

	public void setSzelesseg(int _szelesseg) {
		this.szelesseg = _szelesseg;
	}

	public void setMagassag(int _magassag) {
		this.magassag = _magassag;
	}

	public int getTerulet() {
		return szelesseg*magassag;
	}

}

class Negyzet extends Teglalap {

	public void setSzelesseg(int _szelesseg) {
		this.szelesseg = _szelesseg;
		this.magassag = _szelesseg;
	}

	public void setMagassag(int _magassag) {
		this.szelesseg = _magassag;
		this.magassag = _magassag;
	}

	public int getTerulet() {
		return szelesseg*szelesseg;
	}

}


public class liskov {


	public static void main(String args[]) {

		Teglalap t1 = new Teglalap();
		t1.setMagassag(5);
		t1.setSzelesseg(10);
		System.out.println(t1.getTerulet());

		Negyzet n1 = new Negyzet();
		n1.setMagassag(5);
		n1.setSzelesseg(10);
		System.out.println(n1.getTerulet());
	}

}
