/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epamtarolo;

import java.util.ArrayList;

/**
 *
 * @author RichY
 */
public class Tarolo {
      int size;
      ArrayList<Integer> tarolt;
      
      public Tarolo(ArrayList<Integer> _adatok) {
            size = _adatok.size();
            this.tarolt = _adatok;
      }
      
      public void addAdat(int _szam) {
            this.tarolt.add(_szam);
            this.size++;
      }
      
      public void kiir() {
            for (int i = 0; i < this.tarolt.size(); i++) {
                  System.out.println(tarolt.get(i));
            }
      }
      
      public void rendezes() {
            ArrayList<Integer> tarolt2 = new ArrayList<Integer>(this.tarolt);
            for (int i = tarolt2.size(); i >= 0; i--) {
                  for (int j = 0; j < i-1; j++) {
                        if (tarolt2.get(j) > tarolt2.get(j+1)) {
                              int a = tarolt2.get(j);
                              int b = tarolt2.get(j+1);
                              tarolt2.set(j, b);
                              tarolt2.set(j+1, a);
                        }
                  }
            }
            for (int i = 0; i < tarolt2.size(); i++) {
                  System.out.println(tarolt2.get(i));
            }
      }
      
      public ArrayList<Integer> getRendezett() {
            ArrayList<Integer> tarolt2 = new ArrayList<Integer>(this.tarolt);
            for (int i = tarolt2.size(); i >= 0; i--) {
                  for (int j = 0; j < i-1; j++) {
                        if (tarolt2.get(j) > tarolt2.get(j+1)) {
                              int a = tarolt2.get(j);
                              int b = tarolt2.get(j+1);
                              tarolt2.set(j, b);
                              tarolt2.set(j+1, a);
                        }
                  }
            }

            return tarolt2;
      }
      
      public boolean contains(int _szam) {
            ArrayList<Integer> tarolt2 = new ArrayList<Integer>(this.getRendezett());
            
            int elso = 0;
            int utolso = tarolt2.size()-1;
            
            while (elso <= utolso) {
                  int kozepso = elso + (utolso - elso) / 2;
                  if  (_szam < tarolt2.get(kozepso)) {
                        utolso = kozepso-1;
                  } else if (_szam > tarolt2.get(kozepso)) {
                        elso = kozepso+1;
                  } else {
                        return true;
                  }
            }
            
            return false;
      }
}
