/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor..
 */
package javafx_fullscreen;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import static javafx.scene.input.KeyCode.ESCAPE;
import static javafx.scene.input.KeyCode.F;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 *
 * @author RichY
 */
public class JavaFX_FullScreen extends Application {
      
      @Override
      public void start(Stage stage) throws Exception {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("FXMLDocument.fxml"));
            Parent root = loader.load();
            
            FXMLDocumentController vezerlo = loader.getController();
            
            Scene scene = new Scene(root);
            
            stage.setScene(scene);
            stage.show();
            
            scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            
                  @Override
                  public void handle (KeyEvent keyevent) {
                        if (keyevent.getCode() == F) {
                              stage.setFullScreen(true);
                              vezerlo.fscreenSet(true);
                        }
                        if (keyevent.getCode() == ESCAPE ) {
                              vezerlo.fscreenSet(false);
                        }
                  }
            });
      }

      /**
       * @param args the command line arguments
       */
      public static void main(String[] args) {
            launch(args);
      }
      
}
