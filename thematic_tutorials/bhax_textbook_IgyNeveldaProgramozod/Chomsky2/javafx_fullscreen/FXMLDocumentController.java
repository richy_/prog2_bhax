/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafx_fullscreen;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import static javafx.scene.input.KeyCode.F;
import javafx.scene.input.KeyEvent;

/**
 *
 * @author RichY
 */
public class FXMLDocumentController implements Initializable {
      
      @FXML
      private Label label_fscreen;
      
      public void fscreenSet(boolean value) {
            if (value == true) {
                  label_fscreen.setText("FULLSCREEN!!! (:");
            } else {
                  label_fscreen.setText("Nem fullscreen... ):");
            }
      }     
      
      @Override
      public void initialize(URL url, ResourceBundle rb) {
            // TODO
      }      
      
}
