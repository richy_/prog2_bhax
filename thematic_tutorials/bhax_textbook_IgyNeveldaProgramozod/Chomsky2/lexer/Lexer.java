/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexer;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author RichY
 */
public class Lexer {
      private static Map<String, String[]> lex_keszlet =
            new HashMap<String, String[]>() {{
                    put("A", new String[] { "4", "/-\\", "@", "/\\" });
                    put("B", new String[] { "8", "|3", "|}", "ß" });
                    put("C", new String[] { "<", "c", "©", "[" });
                    put("D", new String[] { "d", "|)", "o|", "|O" });
                    put("E", new String[] { "3", "e", "€", "3|" });
                    put("F", new String[] { "|=", "ph", "|#", "|\"" });
                    put("G", new String[] { "g", "6", "C-", "[+" });
                    put("H", new String[] { "|-|", "[-]", "(-)", "|+|" });
                    put("I", new String[] { "1", "|", "!", "i" });
                    put("J", new String[] { "_|", "_/", "7", "_)"});
                    put("K", new String[] { "|<", "|<", "1<", "|{" });
                    put("L", new String[] { "|_", "|", "1", "][" });
                    put("M", new String[] { "|V|", "/\\/\\", "/x\\", "N\\" });
                    put("N", new String[] { "|\\|", "/V", "и", "п" });
                    put("O", new String[] { "0", "()", "o", "[]" });
                    put("P", new String[] { "|o", "|O", "|>", "|*" });
                    put("Q", new String[] { "O_", "q", "(,)", "o." });
                    put("R", new String[] { "|2", "Я", "r", "®" });
                    put("S", new String[] { "5", "$", "§", "s"});
                    put("T", new String[] { "7", "'|'", "']['", "т" });
                    put("U", new String[] { "|_|", "/_/", "(_)", "[_]"});
                    put("V", new String[] { "\\/", "v", "V", "u" });
                    put("W", new String[] { "\\/\\/", "(/\\)", "|/\\|", "VV" });
                    put("X", new String[] { "%", "><", "}{", "Ж" });
                    put("Y", new String[] { "\\|/", "y", "Ч", "'/"});
                    put("Z", new String[] { "2", "5", "7_", "z" });                    
            }};
      
      public static int randomIndex(int n) {
            return (int) (Math.random() * n);
      }
            
      public static String generate(String input) {
            StringBuilder sb = new StringBuilder();
            
            input = input.toUpperCase();
            for (int i = 0; i < input.length(); i++) {
                 boolean found = false;
                 for (String karakter : lex_keszlet.keySet()) {
                        if (input.charAt(i) == karakter.charAt(0)) {
                              sb.append(lex_keszlet.get(karakter)[randomIndex(3)]);
                              found = true;
                        }
                 }
                 if (found == false) {
                       sb.append(input.charAt(i));
                 }
                  
            }
            return sb.toString();
      }
      
      
}
